"""The main controller for the faces-SRGAN code."""
import os
import numpy as np
import tensorflow as tf
import cv2
import matplotlib.pyplot as plt
from tqdm import tqdm
from srgan import SRGAN


# global constant values
learning_rate = 1e-3
batch_size = 16
vgg_model = '../vgg/backup/latest'
srgan_model = 'backup/latest'


def load_lfw():
    """Load the lfw faces dataset."""
    x_train = np.load('lfw/data/npy/x_train.npy')
    x_test = np.load('lfw/data/npy/x_test.npy')
    return x_train, x_test


def save_triptic(imgs, label, id):
    """Plot the three as a row of captioned images."""
    with plt.xkcd():
        for i in range(batch_size):
            fig = plt.figure()
            for j, img in enumerate(imgs):
                im = np.uint8((img[i]+1)*127.5)
                im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
                fig.add_subplot(1, len(imgs), j+1)
                plt.imshow(im)
                plt.tick_params(labelbottom='off')
                plt.tick_params(labelleft='off')
                plt.gca().get_xaxis().set_ticks_position('none')
                plt.gca().get_yaxis().set_ticks_position('none')
                plt.xlabel(label[j])
            seq_ = "{0:05d}".format(i+1)
            epoch_ = "{0:05d}".format(id)
            path = os.path.join('../results', seq_, '{}.jpg'.format(epoch_))
            if os.path.exists(os.path.join('../results', seq_)) is False:
                os.mkdir(os.path.join('../results', seq_))
            plt.savefig(path)
            plt.close()


def plot_loss(track_d_loss, track_g_loss, test_val):
    """Plot the loss plots with smoothing."""
    def smooth(y, box_pts):
        box = np.ones(box_pts)/box_pts
        y_smooth = np.convolve(y, box, mode='same')
        return y_smooth
    xs = [_ for _ in range(len(track_d_loss))]
    w = 3
    # xkcd styling
    with plt.xkcd():
        f, axs = plt.subplots(2, figsize=(8, 4), sharex=True)
        axs[0].plot(track_d_loss, alpha=0.3, linewidth=5)
        axs[0].plot(xs[w:-w], smooth(track_d_loss, w)[w:-w], c='C0')
        axs[0].set_title('Discriminator', fontsize=10)
        axs[0].set_yscale('log')
        axs[0].set_ylabel('loss', fontsize=10)
        plt.xkcd()
        axs[1].plot(track_g_loss, alpha=0.3, linewidth=5, c='C4')
        axs[1].plot(xs[w:-w], smooth(track_g_loss, w)[w:-w], c='C4')
        axs[1].set_yscale('log')
        axs[1].set_title('Generator', fontsize=10)
        axs[1].set_xlabel('Epoch', fontsize=10)
        axs[1].set_ylabel('loss', fontsize=10)
    plt.tight_layout()
    if test_val % 250 == 0:
        extra_name = str(test_val)
    else:
        extra_name = ''
    plt.savefig('../results/'+'loss_tracking{}.png'.format(extra_name))
    plt.close()


def normalize(images):
    """Normalize the iamges."""
    return np.array([image/127.5-1 for image in images])


def train(TRAIN=True):
    """Set up the training method and losses."""
    # image size 96x96x3 colour channels
    x = tf.placeholder(tf.float32, [None, 96, 96, 3])
    is_training = tf.placeholder(tf.bool, [])

    # The actual GAN framework goes here
    model = SRGAN(x, is_training, batch_size)
    sess = tf.Session()
    # with tf.variable_scope('srgan'):
    #     global_step = tf.Variable(0, name='global_step', trainable=False)
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
    g_train_op = optimizer.minimize(model.g_loss, #global_step=global_step,
                                    var_list=model.g_variables)
    d_train_op = optimizer.minimize(model.d_loss, #global_step=global_step,
                                    var_list=model.d_variables)
    init = tf.global_variables_initializer()
    sess.run(init)

    # load the vgg netowrk weights
    var = tf.global_variables()
    vgg_var = [var_ for var_ in var if 'vgg19' in var_.name]
    saver = tf.train.Saver(vgg_var)
    saver.restore(sess, vgg_model)

    # load the SRGAN network weights to continue training
    if TRAIN is False:
        saver = tf.train.Saver()
        saver.restore(sess, srgan_model)

    # load the data
    x_train, x_test = load_lfw()

    # Train the SRGAN model
    n_batches = int(len(x_train) / batch_size)
    epoch = 0
    track_d_loss = []
    track_g_loss = []
    plot_scheduler = 1  # how frequently to plot (in batches)
    while True:
        epoch += 1
        print('epoch:', epoch)
        np.random.shuffle(x_train)  # shuffle list in place
        for i in tqdm(range(n_batches)):
            x_batch = normalize(x_train[i*batch_size:(i+1)*batch_size])
            _, _, gloss, dloss = sess.run([g_train_op, d_train_op,
                                           model.g_loss, model.d_loss],
                                          feed_dict={x: x_batch,
                                                     is_training: True})
            track_d_loss.append(dloss)
            track_g_loss.append(gloss)
            test_val = (epoch-1)*n_batches + i
            if i % plot_scheduler == 0:
                # Then plot the loss in a appealing format
                if i > 1:  # Ensures there is data to plot
                    plot_loss(track_d_loss, track_g_loss, test_val)
            # validate
            raw = normalize(x_test[:batch_size])  # note we always use the same
            mos, fake = sess.run([model.downscaled, model.imitation],
                                 feed_dict={x: raw, is_training: False})
            save_triptic([mos, fake, raw],
                         ['Input', 'Output', 'Original'], test_val)
            # save the model after each batch
            saver = tf.train.Saver()
            saver.save(sess, 'backup/latest', write_meta_graph=False)


if __name__ == '__main__':
    train()
